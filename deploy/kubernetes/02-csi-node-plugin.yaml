# SPDX-License-Identifier: Apache-2.0

apiVersion: v1
kind: ServiceAccount
metadata:
  name: csi-node-plugin
  namespace: kubesan

---

apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: csi-node-plugin
  namespace: kubesan
spec:
  selector:
    matchLabels: &labels
      kubesan.gitlab.io/component: csi-node-plugin
  template:
    metadata:
      labels: *labels
    spec:
      serviceAccountName: csi-node-plugin
      hostPID: true
      containers:
        - name: csi-plugin
          image: &image quay.io/kubesan/kubesan:latest
          command:
            - ./kubesan
            - csi-node-plugin
          env:
            - name: KUBESAN_IMAGE
              value: *image
            - name: NODE_NAME
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName
          volumeMounts:
            - name: kubelet-dir
              mountPropagation: Bidirectional # for Filesystem volume mounting
              mountPath: /var/lib/kubelet
            - name: socket-dir
              mountPath: /run/csi
            - name: dev
              mountPath: /dev
            - name: run-kubesan-nbd
              mountPath: /run/kubesan/nbd
          securityContext:
            privileged: true
        - name: node-driver-registrar
          image: registry.k8s.io/sig-storage/csi-node-driver-registrar:v2.8.0
          args:
            - --kubelet-registration-path=/var/lib/kubelet/plugins/kubesan/socket
          volumeMounts:
            - name: registration-dir
              mountPath: /registration
            - name: socket-dir
              mountPath: /run/csi
      volumes:
        # where kubelet wants volumes to be staged/published
        - name: kubelet-dir
          hostPath:
            path: /var/lib/kubelet
            type: Directory
        # where node-driver-registrar registers the plugin with kubelet
        - name: registration-dir
          hostPath:
            path: /var/lib/kubelet/plugins_registry
            type: Directory
        # where the socket for kubelet <-> plugin communication is created
        - name: socket-dir
          hostPath:
            path: /var/lib/kubelet/plugins/kubesan
            type: DirectoryOrCreate
        - name: dev
          hostPath:
            path: /dev
            type: Directory
        - name: run-kubesan-nbd
          hostPath:
            path: /run/kubesan/nbd
            type: DirectoryOrCreate
