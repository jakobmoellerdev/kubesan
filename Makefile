# SPDX-License-Identifier: Apache-2.0

PROJECT_DIR := $(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))
MODULE_NAME := $(shell grep '^module' $(PROJECT_DIR)/go.mod | awk '{print $$2}')

CONTROLLER_TOOLS_VERSION := 0.15.0
K8S_CONFORMANCE := 0.30.3
IMG ?= kubesan:latest

.PHONY: build
build:
	podman image build --tag $(IMG) .

.PHONY: fmt
fmt:
	go fmt ./...

.PHONY: vet
vet:
	go vet ./...

.PHONY: check
check:
	cd tests && ./run.sh all

generate: generate-crds

generate-crds: controller-gen
	$(CONTROLLER_GEN) crd object paths="./..."
	rm deploy/kubernetes/00-crd.yaml
	cat $(PROJECT_DIR)/config/crd/kubesan.gitlab.io_blobpools.yaml >> deploy/kubernetes/00-crd.yaml
	rm -r $(PROJECT_DIR)/config/crd

generate-client: client-gen applyconfiguration-gen
	$(CLIENT_GEN) \
		--input-base $(PROJECT_DIR)/pkg/api \
		--output-dir $(PROJECT_DIR)/pkg/client \
		--output-pkg $(MODULE_NAME)/pkg/client \
		--input blobpools/v1alpha1 \
		--clientset-name versioned \
		--apply-configuration-package $(MODULE_NAME)/pkg/client/applyconfigurations
	$(APPLYCONFIGURATION_GEN) \
		$(PROJECT_DIR)/pkg/api/blobpools/v1alpha1 \
		--output-dir $(PROJECT_DIR)/pkg/client/applyconfigurations \
		--output-pkg $(MODULE_NAME)/pkg/client/applyconfigurations

CONTROLLER_GEN = $(PROJECT_DIR)/bin/controller-gen
controller-gen: ## Download controller-gen locally if necessary.
	$(call go-get-tool,$(CONTROLLER_GEN),sigs.k8s.io/controller-tools/cmd/controller-gen@v$(CONTROLLER_TOOLS_VERSION))

CLIENT_GEN = $(PROJECT_DIR)/bin/client-gen
client-gen: ## Download controller-gen locally if necessary.
	$(call go-get-tool,$(CLIENT_GEN),k8s.io/code-generator/cmd/client-gen@v$(K8S_CONFORMANCE))

APPLYCONFIGURATION_GEN = $(PROJECT_DIR)/bin/applyconfiguration-gen
applyconfiguration-gen: ## Download controller-gen locally if necessary.
	$(call go-get-tool,$(APPLYCONFIGURATION_GEN),k8s.io/code-generator/cmd/applyconfiguration-gen@v$(K8S_CONFORMANCE))

# go-get-tool will 'go get' any package $2 and install it to $1.
define go-get-tool
@[ -f $(1) ] || echo "Downloading $(2)"; GOBIN=$(PROJECT_DIR)/bin go install -mod=readonly $(2)
endef
