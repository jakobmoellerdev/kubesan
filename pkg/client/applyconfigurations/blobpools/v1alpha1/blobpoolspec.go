// Code generated by applyconfiguration-gen. DO NOT EDIT.

package v1alpha1

// BlobPoolSpecApplyConfiguration represents an declarative configuration of the BlobPoolSpec type for use
// with apply.
type BlobPoolSpecApplyConfiguration struct {
	Blobs        []string                           `json:"blobs,omitempty"`
	ActiveOnNode *string                            `json:"activeOnNode,omitempty"`
	Holders      []BlobPoolHolderApplyConfiguration `json:"holders,omitempty"`
}

// BlobPoolSpecApplyConfiguration constructs an declarative configuration of the BlobPoolSpec type for use with
// apply.
func BlobPoolSpec() *BlobPoolSpecApplyConfiguration {
	return &BlobPoolSpecApplyConfiguration{}
}

// WithBlobs adds the given value to the Blobs field in the declarative configuration
// and returns the receiver, so that objects can be build by chaining "With" function invocations.
// If called multiple times, values provided by each call will be appended to the Blobs field.
func (b *BlobPoolSpecApplyConfiguration) WithBlobs(values ...string) *BlobPoolSpecApplyConfiguration {
	for i := range values {
		b.Blobs = append(b.Blobs, values[i])
	}
	return b
}

// WithActiveOnNode sets the ActiveOnNode field in the declarative configuration to the given value
// and returns the receiver, so that objects can be built by chaining "With" function invocations.
// If called multiple times, the ActiveOnNode field is set to the value of the last call.
func (b *BlobPoolSpecApplyConfiguration) WithActiveOnNode(value string) *BlobPoolSpecApplyConfiguration {
	b.ActiveOnNode = &value
	return b
}

// WithHolders adds the given value to the Holders field in the declarative configuration
// and returns the receiver, so that objects can be build by chaining "With" function invocations.
// If called multiple times, values provided by each call will be appended to the Holders field.
func (b *BlobPoolSpecApplyConfiguration) WithHolders(values ...*BlobPoolHolderApplyConfiguration) *BlobPoolSpecApplyConfiguration {
	for i := range values {
		if values[i] == nil {
			panic("nil value passed to WithHolders")
		}
		b.Holders = append(b.Holders, *values[i])
	}
	return b
}
