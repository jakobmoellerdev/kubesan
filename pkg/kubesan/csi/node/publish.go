// SPDX-License-Identifier: Apache-2.0

package node

import (
	"context"
	"os"

	"github.com/container-storage-interface/spec/lib/go/csi"
	"gitlab.com/kubesan/kubesan/pkg/kubesan/util/util"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *NodeServer) NodePublishVolume(ctx context.Context, req *csi.NodePublishVolumeRequest) (*csi.NodePublishVolumeResponse, error) {
	// Check arguments
	if req.GetVolumeCapability() == nil {
		return nil, status.Error(codes.InvalidArgument, "Volume capability missing in request")
	}
	if len(req.GetVolumeId()) == 0 {
		return nil, status.Error(codes.InvalidArgument, "Volume ID missing in request")
	}
	if len(req.GetTargetPath()) == 0 {
		return nil, status.Error(codes.InvalidArgument, "Target path missing in request")
	}

	// TODO: Validate request.

	err := util.Symlink(req.StagingTargetPath, req.TargetPath)
	if err != nil {
		return nil, err
	}

	resp := &csi.NodePublishVolumeResponse{}
	return resp, nil
}

func (s *NodeServer) NodeUnpublishVolume(ctx context.Context, req *csi.NodeUnpublishVolumeRequest) (*csi.NodeUnpublishVolumeResponse, error) {
	if len(req.GetVolumeId()) == 0 {
		return nil, status.Error(codes.InvalidArgument, "Volume ID missing in request")
	}
	if len(req.GetTargetPath()) == 0 {
		return nil, status.Error(codes.InvalidArgument, "Target path missing in request")
	}

	// TODO: Validate request.
	resp := &csi.NodeUnpublishVolumeResponse{}

	_, err := os.Lstat(req.TargetPath)
	if os.IsNotExist(err) {
		return resp, nil
	}

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if err := os.Remove(req.TargetPath); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return resp, nil
}
