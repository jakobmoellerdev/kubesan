// SPDX-License-Identifier: Apache-2.0

package blobs

import (
	"context"
	"errors"
	"fmt"
	"log"

	"gitlab.com/kubesan/kubesan/pkg/api/blobpools/v1alpha1"
	"gitlab.com/kubesan/kubesan/pkg/kubesan/util/config"
	"gitlab.com/kubesan/kubesan/pkg/kubesan/util/nbd"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Ensure that the given blob is attached on the given node (or any node if `node` is nil).
//
// If `node` is nil, this will select a node where the blob gets a "fast" attachment.
//
// Cookies are "namespaced" to the (blob, node) pair.
//
// This method is idempotent and may be called from any node.
func (bm *BlobManager) AttachBlob(
	ctx context.Context, blob *Blob, node *string, cookie string,
) (actualNode string, path string, err error) {
	pool, err := bm.Clientset().BlobPools().Get(ctx, blob.pool.name, metav1.GetOptions{})
	if err != nil {
		return
	}

	if node != nil {
		actualNode = *node
	} else if pool.Spec.ActiveOnNode != nil {
		actualNode = *pool.Spec.ActiveOnNode
	} else {
		actualNode = config.LocalNodeName
	}

	log.Println(fmt.Sprintf("attaching blob %s on current node %s", blob, actualNode))
	path, err = bm.attachBlob(ctx, blob, &pool.Spec, actualNode, cookie)
	if err != nil {
		log.Println(fmt.Sprintf("failed to attach blob %s on current node %s: %v", blob, actualNode, err))
	}
	return
}

// Ensure that the given blob is attached on the given node (or any node if `node` is nil).
//
// If `node` is nil, this will select a node where the blob gets a "fast" attachment.
//
// Cookies are "namespaced" to the (blob, node) pair.
//
// This method is idempotent and may be called from any node.
func (bm *BlobManager) attachBlob(
	ctx context.Context,
	blob *Blob,
	poolSpec *v1alpha1.BlobPoolSpec,
	actualNode string,
	cookie string,
) (path string, err error) {
	var activeOnNode string

	if poolSpec.ActiveOnNode == nil {
		activeOnNode = actualNode
		// activate LVM thin *pool* LV
		if err = bm.runLvmScriptForThinPoolLv(ctx, blob.pool, actualNode, "activate-pool"); err != nil {
			err = status.Errorf(codes.Internal, "failed to activate LVM thin pool LV: %s", err)
			return
		}
	} else {
		activeOnNode = *poolSpec.ActiveOnNode
	}

	path = blob.dmMultipathVolumePath()

	if poolSpec.HasHolder(blob.name, actualNode, cookie) {
		log.Println(fmt.Sprintf("blob %s is already attached on %s, skipping another attach", blob.name, actualNode))
		return
	}

	if !poolSpec.HasHolderForBlobOnNode(blob.name, actualNode) {
		var lvmOrNbdPath string

		if activeOnNode == actualNode {
			// activate LVM thin LV
			if err = bm.runLvmScriptForThinLv(ctx, blob, actualNode, "activate"); err != nil {
				err = status.Errorf(codes.Internal, "failed to activate LVM thin LV: %s", err)
				return
			}

			lvmOrNbdPath = blob.lvmThinLvPath()
		} else {
			nbdServerId := &nbd.ServerId{
				NodeName: activeOnNode,
				BlobName: blob.name,
			}

			err = nbd.StartServer(ctx, bm.clientset, nbdServerId, blob.lvmThinLvPath())
			if err != nil {
				log.Println(fmt.Sprintf("Failed to start NBD server for %s on %s: %v", blob.name, activeOnNode, err))
				return
			}

			// connect NBD client
			lvmOrNbdPath, err = nbd.ConnectClient(ctx, bm.clientset, actualNode, nbdServerId)
			if err != nil {
				log.Println(fmt.Sprintf("Failed to connect NBD client to %s on %s: %v", nbdServerId.Hostname(), actualNode, err))
				return
			}

			log.Println(fmt.Sprintf("Connected NBD client to %s on %s and make path available at %s", nbdServerId.Hostname(), actualNode, lvmOrNbdPath))
		}

		// create dm-multipath volume
		log.Println(fmt.Sprintf("Creating dm-multipath volume for %s on %s", blob.name, actualNode))
		err = bm.runDmMultipathScript(ctx, blob, actualNode, "create", lvmOrNbdPath)
		if err != nil {
			log.Println(fmt.Sprintf("Failed to create dm-multipath volume for %s on %s: %v", blob.name, actualNode, err))
			return
		}
		log.Println(fmt.Sprintf("Created dm-multipath volume for %s on %s", blob.name, actualNode))
	}

	// TODO: For now we assume that the state hasn't changed since we checked it at the beginning of this method.

	err = bm.atomicUpdateBlobPoolCrd(ctx, blob.pool.name, func(poolSpec *v1alpha1.BlobPoolSpec) error {
		poolSpec.ActiveOnNode = &activeOnNode
		poolSpec.AddHolder(blob.name, actualNode, cookie)
		return nil
	})

	return
}

// The reverse of AttachBlob().
//
// Each call to AttachBlob() must be paired with a call to DetachBlob() with the same `cookie`.
//
// This method is idempotent and may be called from any node.
func (bm *BlobManager) DetachBlob(ctx context.Context, blob *Blob, node string, cookie string) error {
	blobPool, err := bm.Clientset().BlobPools().Get(ctx, blob.pool.name, metav1.GetOptions{})
	if k8serrors.IsNotFound(err) {
		log.Println(fmt.Sprintf("blob pool %s not found, skipping and assuming already detached", blob.pool.name))
		return nil
	}
	if err != nil {
		return err
	}

	if node == "" && cookie == "" && len(blobPool.Spec.Holders) > 0 {
		log.Println(fmt.Sprintf("detaching blob %s from all nodes", blob.name))
		var err error
		for _, holder := range blobPool.Spec.Holders {
			err = errors.Join(err, bm.DetachBlob(ctx, blob, holder.Node, holder.Cookie))
		}
		return err
	}

	if !blobPool.Spec.HasHolder(blob.name, node, cookie) {
		log.Println(fmt.Sprintf("blob %s is not attached on %s, skipping another detach", blob.name, node))
		return nil
	} else {
		log.Println(fmt.Sprintf("detaching blob %s from %s", blob.name, node))
		blobPool.Spec.RemoveHolder(blob.name, node, cookie)
	}

	if !blobPool.Spec.HasHolderForBlobOnNode(blob.name, node) {
		log.Println(fmt.Sprintf("removing multipath device for %s on %s", blob.name, node))
		if err = bm.runDmMultipathScript(ctx, blob, node, "remove"); err != nil {
			return status.Errorf(codes.Internal, "failed to remove dm-multipath device: %s", err)
		}

		nbdServerId := &nbd.ServerId{
			NodeName: *blobPool.Spec.ActiveOnNode,
			BlobName: blob.name,
		}

		if node != *blobPool.Spec.ActiveOnNode {
			if err = nbd.DisconnectClient(ctx, bm.clientset, node, nbdServerId); err != nil {
				return status.Errorf(codes.Internal, "failed to disconnect NBD client: %s", err)
			}
		}

		if !blobPool.Spec.HasHolderForBlob(blob.name) {
			err = nbd.StopServer(ctx, bm.clientset, nbdServerId)
			if err != nil {
				return err
			}

			// deactivate LVM thin LV

			err = bm.runLvmScriptForThinLv(ctx, blob, *blobPool.Spec.ActiveOnNode, "deactivate")
			if err != nil {
				return status.Errorf(codes.Internal, "failed to deactivate LVM thin LV: %s", err)
			}
		} else {
			log.Println(fmt.Sprintf("blob %s is still attached on %s, skipping nbd server shutdown", blob.name, blobPool.Spec.NodesWithHoldersForBlob(blob.name)))
		}

		if !blobPool.Spec.HasHolderOnNode(*blobPool.Spec.ActiveOnNode) {
			// `node` where the LVM thin *pool* LV is active no longer needs access to the pool
			log.Println(fmt.Sprintf("deactivating LVM thin pool LV for %s on %s as node no longer needs access", blob.pool.name, *blobPool.Spec.ActiveOnNode))
			if len(blobPool.Spec.Holders) > 0 {
				candidate := blobPool.Spec.Holders[0].Node
				log.Println(fmt.Sprintf("migrating LVM thin pool LV for %s on %s to the next possible node %s", blob.pool.name, *blobPool.Spec.ActiveOnNode, candidate))
				// migrate to any node where the pool is already attached
				err = bm.migratePool(ctx, blob.pool, &blobPool.Spec, candidate)
				if err != nil {
					return err
				}
			} else {
				// deactivate LVM thin *pool* LV
				log.Println(fmt.Sprintf("deactivating LVM thin pool LV for %s on %s as no node currently needs access", blob.pool.name, *blobPool.Spec.ActiveOnNode))
				err := bm.runLvmScriptForThinPoolLv(ctx, blob.pool, *blobPool.Spec.ActiveOnNode, "deactivate-pool")
				if err != nil {
					return status.Errorf(codes.Internal, "failed to deactivate LVM thin pool LV: %s", err)
				}
			}
		}

	}

	// TODO: For now we assume that the state hasn't changed since we checked it at the beginning of this method.
	err = bm.atomicUpdateBlobPoolCrd(ctx, blob.pool.name, func(poolSpec *v1alpha1.BlobPoolSpec) error {
		poolSpec.RemoveHolder(blob.name, node, cookie)
		if len(poolSpec.Holders) == 0 {
			poolSpec.ActiveOnNode = nil
		}
		return nil
	})
	if err != nil {
		return err
	}

	return nil
}
