// SPDX-License-Identifier: Apache-2.0

package blobs

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/kubesan/kubesan/pkg/api/blobpools/v1alpha1"
	v1alpha1client "gitlab.com/kubesan/kubesan/pkg/client/versioned/typed/blobpools/v1alpha1"
	"gitlab.com/kubesan/kubesan/pkg/kubesan/util/config"
	"gitlab.com/kubesan/kubesan/pkg/kubesan/util/k8s"
	"gitlab.com/kubesan/kubesan/pkg/kubesan/util/lvm"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
)

type BlobManagerClientSet interface {
	v1alpha1client.BlobpoolsV1alpha1Interface
	kubernetes.Interface
}

type blobManagerClientSet struct {
	v1alpha1client.BlobpoolsV1alpha1Interface
	kubernetes.Interface
}

type BlobManager struct {
	clientset BlobManagerClientSet
}

// KubeSAN VGs use their own LVM profile to avoid interfering with the
// system-wide lvm.conf configuration. This profile is hardcoded here and is
// put in place before creating LVs that get their configuration from the
// profile.
func setUpLvmProfile() error {
	return lvm.WriteProfile("kubesan",
		`# This file is part of the KubeSAN CSI plugin and may be automatically
# updated. Do not edit!

activation {
        thin_pool_autoextend_threshold=95
        thin_pool_autoextend_percent=20
}
`)
}

func NewBlobManager() (*BlobManager, error) {
	err := setUpLvmProfile()
	if err != nil {
		return nil, err
	}

	restConfig, err := rest.InClusterConfig()
	if err != nil {
		return nil, err
	}

	crdRestConfig := rest.CopyConfig(restConfig)
	crdRestConfig.GroupVersion = &schema.GroupVersion{Group: config.Domain, Version: "v1alpha1"}
	crdRestConfig.NegotiatedSerializer = scheme.Codecs.WithoutConversion()
	crdRestConfig.APIPath = "/apis"
	if crdRestConfig.UserAgent == "" {
		crdRestConfig.UserAgent = rest.DefaultKubernetesUserAgent()
	}
	httpClient, err := rest.HTTPClientFor(crdRestConfig)
	if err != nil {
		return nil, err
	}

	blobclientset, err := v1alpha1client.NewForConfigAndClient(restConfig, httpClient)
	if err != nil {
		return nil, err
	}
	clientset, err := kubernetes.NewForConfigAndClient(restConfig, httpClient)
	if err != nil {
		return nil, err
	}

	return &BlobManager{
		clientset: &blobManagerClientSet{
			BlobpoolsV1alpha1Interface: blobclientset,
			Interface:                  clientset,
		},
	}, nil
}

func (bm *BlobManager) Clientset() BlobManagerClientSet {
	return bm.clientset
}

// This method may be called from any node, and fails if the blob does not exist.
func (bm *BlobManager) GetBlobSize(ctx context.Context, blob *Blob) (int64, error) {
	output, err := lvm.Command(
		ctx,
		"lvs",
		"--devicesfile", blob.pool.backingVolumeGroup,
		"--options", "lv_size",
		"--units", "b",
		"--nosuffix",
		"--noheadings",
		fmt.Sprintf("%s/%s", blob.pool.backingVolumeGroup, blob.lvmThinLvName()),
	)
	if err != nil {
		return -1, status.Errorf(codes.Internal, "failed to get size of LVM LV: %s: %s", err, output)
	}

	sizeStr := strings.TrimSpace(output)

	size, err := strconv.ParseInt(sizeStr, 0, 64)
	if err != nil {
		return -1, status.Errorf(codes.Internal, "failed to get size of LVM LV: %s: %s", err, sizeStr)
	}

	return size, nil
}

func (bm *BlobManager) atomicUpdateBlobPoolCrd(ctx context.Context, poolName string, f func(*v1alpha1.BlobPoolSpec) error) error {
	crd := v1alpha1.BlobPool{ObjectMeta: metav1.ObjectMeta{Name: poolName}}

	err := k8s.AtomicUpdate(
		ctx, bm.clientset.RESTClient(), "blobpools", &crd,
		func(crd *v1alpha1.BlobPool) error { return f(&crd.Spec) },
	)
	if err != nil {
		return err
	}

	return nil
}

func (bm *BlobManager) createBlobPoolCrd(ctx context.Context, poolName string, poolSpec *v1alpha1.BlobPoolSpec) error {
	crd := v1alpha1.BlobPool{
		ObjectMeta: metav1.ObjectMeta{Name: poolName},
		Spec:       *poolSpec,
	}
	if crd.Spec.Blobs == nil {
		crd.Spec.Blobs = []string{}
	}

	if crd.Spec.Holders == nil {
		crd.Spec.Holders = []v1alpha1.BlobPoolHolder{}
	}

	_, err := bm.Clientset().BlobPools().Create(ctx, &crd, metav1.CreateOptions{})

	if err != nil && !k8serrors.IsAlreadyExists(err) {
		return err
	}

	return nil
}

func (bm *BlobManager) deleteBlobPoolCrd(ctx context.Context, poolName string) error {
	err := bm.Clientset().BlobPools().Delete(ctx, poolName, metav1.DeleteOptions{})

	if err != nil && !k8serrors.IsNotFound(err) {
		return err
	}

	return nil
}
