// SPDX-License-Identifier: Apache-2.0

package blobs

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/kubesan/kubesan/pkg/api/blobpools/v1alpha1"
	"gitlab.com/kubesan/kubesan/pkg/kubesan/util/nbd"
	"gitlab.com/kubesan/kubesan/pkg/kubesan/util/slices"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Adjusts blob attachments such that `blob` has the best I/O performance on `node`.
//
// This may affect the performance of blobs the were (transitively) copied from `blob` or from which `blob` was copied.
//
// Does nothing if the blob isn't attached to any node.
func (bm *BlobManager) OptimizeBlobAttachmentForNode(ctx context.Context, blob *Blob, node string) error {
	pool, err := bm.Clientset().BlobPools().Get(ctx, blob.pool.name, metav1.GetOptions{})
	if err != nil {
		return err
	}

	err = bm.migratePool(ctx, blob.pool, &pool.Spec, node)
	if err != nil {
		return err
	}

	return nil
}

// Works even if the pool has no holders on `toNode`.
//
// Does nothing if the pool has no holders at all.
func (bm *BlobManager) migratePool(ctx context.Context, pool *internalBlobPool, poolSpec *v1alpha1.BlobPoolSpec, toNode string) error {
	if poolSpec.ActiveOnNode == nil {
		// nothing to do
		log.Println("pool has no active node, nothing to migrate")
		return nil
	}
	if *poolSpec.ActiveOnNode == toNode {
		log.Println(fmt.Sprintf("pool is already active on node %s, nothing to migrate", toNode))
		// nothing to do
		return nil
	}

	// proactively start LVM VG lockspace on `toNode`, which can take a while, otherwise migration may take too long
	log.Println(fmt.Sprintf("proactively starting LVM VG lockspace on node %s", toNode))
	err := bm.runLvmScriptForThinPoolLv(ctx, pool, toNode, "lockstart")
	if err != nil {
		return err
	}

	log.Println(fmt.Sprintf("migrating pool %s from node %s to node %s", pool.name, *poolSpec.ActiveOnNode, toNode))

	err = bm.migratePoolDown(ctx, pool, poolSpec)
	if err != nil {
		return err
	}

	err = bm.migratePoolUp(ctx, pool, poolSpec, toNode)
	if err != nil {
		return err
	}

	log.Println(fmt.Sprintf("migrated pool %s from node %s to node %s, adjusting activeOnNode", pool.name, *poolSpec.ActiveOnNode, toNode))
	err = bm.atomicUpdateBlobPoolCrd(ctx, pool.name, func(poolSpec *v1alpha1.BlobPoolSpec) error {
		poolSpec.ActiveOnNode = &toNode
		return nil
	})
	if err != nil {
		return err
	}

	return nil
}

func (bm *BlobManager) migratePoolDown(ctx context.Context, pool *internalBlobPool, poolSpec *v1alpha1.BlobPoolSpec) error {
	nodeWithActiveLvmThinPoolLv := *poolSpec.ActiveOnNode

	for _, blobName := range poolSpec.BlobsWithHolders() {
		blob := &Blob{
			name: blobName,
			pool: pool,
		}

		nbdServerId := &nbd.ServerId{
			NodeName: nodeWithActiveLvmThinPoolLv,
			BlobName: blobName,
		}

		for _, node := range poolSpec.NodesWithHoldersForBlob(blobName) {
			log.Println(fmt.Sprintf("disconnecting device for blob %s on node %s", blobName, node))
			if err := bm.runDmMultipathScript(ctx, blob, node, "disconnect"); err != nil {
				return err
			}

			if node != nodeWithActiveLvmThinPoolLv {
				if err := nbd.DisconnectClient(ctx, bm.clientset, node, nbdServerId); err != nil {
					return err
				}
			}
		}

		if err := nbd.StopServer(ctx, bm.clientset, nbdServerId); err != nil {
			return err
		}

		log.Println(fmt.Sprintf("deactivating LVM thin LV %s on node %s", blob.lvmThinLvName(), nodeWithActiveLvmThinPoolLv))
		if err := bm.runLvmScriptForThinLv(ctx, blob, nodeWithActiveLvmThinPoolLv, "deactivate"); err != nil {
			return status.Errorf(codes.Internal, "failed to deactivate LVM thin LV: %s", err)
		}

	}

	// deactivate LVM thin *pool* LV
	log.Println(fmt.Sprintf("deactivating LVM thin pool LV %s on node %s", pool.lvmThinPoolLvName(), nodeWithActiveLvmThinPoolLv))
	err := bm.runLvmScriptForThinPoolLv(ctx, pool, nodeWithActiveLvmThinPoolLv, "deactivate-pool")
	if err != nil {
		return status.Errorf(codes.Internal, "failed to deactivate LVM thin pool LV: %s", err)
	}

	return nil
}

// Assumes that there are holders for the pool.
func (bm *BlobManager) migratePoolUp(
	ctx context.Context, pool *internalBlobPool, poolSpec *v1alpha1.BlobPoolSpec, newNodeWithActiveLvmThinPoolLv string,
) error {
	// activate LVM thin *pool* LV
	log.Println(fmt.Sprintf("activating LVM thin pool LV %s on node %s", pool.lvmThinPoolLvName(), newNodeWithActiveLvmThinPoolLv))
	if err := bm.runLvmScriptForThinPoolLv(ctx, pool, newNodeWithActiveLvmThinPoolLv, "activate-pool"); err != nil {
		return status.Errorf(codes.Internal, "failed to activate LVM thin pool LV: %s", err)
	}

	for _, blobName := range poolSpec.BlobsWithHolders() {
		blob := &Blob{
			name: blobName,
			pool: pool,
		}

		log.Println(fmt.Sprintf("activating LVM thin LV %s on node %s", blob.lvmThinLvName(), newNodeWithActiveLvmThinPoolLv))
		if err := bm.runLvmScriptForThinLv(ctx, blob, newNodeWithActiveLvmThinPoolLv, "activate"); err != nil {
			return status.Errorf(codes.Internal, "failed to activate LVM thin LV: %s", err)
		}

		if poolSpec.HasHolderForBlobOnNode(blobName, newNodeWithActiveLvmThinPoolLv) {
			log.Println(fmt.Sprintf("connecting device for blob %s on node %s", blobName, newNodeWithActiveLvmThinPoolLv))
			if err := bm.runDmMultipathScript(ctx, blob, newNodeWithActiveLvmThinPoolLv, "connect", blob.lvmThinLvPath()); err != nil {
				return err
			}
		}

		otherNodesHoldingBlob := poolSpec.NodesWithHoldersForBlob(blobName)
		slices.Remove(otherNodesHoldingBlob, newNodeWithActiveLvmThinPoolLv)

		if len(otherNodesHoldingBlob) > 0 {
			nbdServerId := &nbd.ServerId{
				NodeName: newNodeWithActiveLvmThinPoolLv,
				BlobName: blobName,
			}
			if err := nbd.StartServer(ctx, bm.clientset, nbdServerId, blob.lvmThinLvPath()); err != nil {
				return err
			}

			for _, node := range otherNodesHoldingBlob {
				nbdDevicePath, err := nbd.ConnectClient(ctx, bm.clientset, node, nbdServerId)
				if err != nil {
					return err
				}

				err = bm.runDmMultipathScript(ctx, blob, node, "connect", nbdDevicePath)
				if err != nil {
					return err
				}
			}
		}
	}

	return nil
}
