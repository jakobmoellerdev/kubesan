package util

import (
	"crypto/sha1"
	"fmt"
	"hash"
	"hash/fnv"
	"math/rand"
	"reflect"
	"runtime"
	"testing"
	"time"
)

func BenchmarkHash(b *testing.B) {
	const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	methods := []func() hash.Hash{
		fnv.New128a,
		fnv.New128,
		sha1.New,
	}
	lengths := []int64{
		16,
		64,
		128,
		256,
		512,
		1024,
		2048,
		4096,
	}

	randSource := rand.NewSource(time.Now().UnixNano())
	randomData := func(length int64) []byte {
		seededRand := rand.New(randSource)
		b := make([]byte, length)
		for i := range b {
			b[i] = charset[seededRand.Intn(len(charset))]
		}
		return b
	}

	getFunctionName := func(i any) string {
		return runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
	}

	run := func(method func() hash.Hash, length int64) {
		data := randomData(length)
		b.Run(fmt.Sprintf("%s(%d)", getFunctionName(method), length), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				m := method()
				in := make([]byte, 0, m.Size())
				_, err := m.Write(data)
				if err != nil {
					b.Fatalf("error writing to hash: %v", err)
				}
				_ = m.Sum(in)
			}
		})
	}

	for _, length := range lengths {
		b.SetBytes(length)
		for _, method := range methods {
			run(method, length)
		}
	}
}
